template = "text.svg"

name = "Nógos I"
blurb = """

<b>Fyzička 4, Finesa 3, Duše 2</b>, Životy 10
<br>Zbroj 1 (kožená zástěra)

<br>
<br><i>Ocelová tyč</i> (Fyzička): 4 + k6, zranění 2
<br>
<br>Jedový dech <i>*Našam rál*</i> (jako ohnivá
koule, ale zraň. <ziravina>žíravinou</ziravina>)
"""
