template = "text.svg"

name = "Nógos III"
blurb = """

<b>Fyzička 2, Finesa 3, Duše 4</b>, Životy 10
<br>

<br>Paralýza <i>*Šituk*</i> ● Přijď <i>*Bo*</i>; střední<br>

<br>Pokud je v kontaktní vzdálenosti od něj prolita krev (u vážného zranění
v krátké), může místo akce ssát: za každý život zranění vyléčí libovolnému
jednomu Nógos 1kz.

"""
