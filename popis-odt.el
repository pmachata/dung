;; To find what plist the in-file options expand to, evaluate the following
;; in-buffer:
;;  (org-export--get-inbuffer-options 'org)

(org-odt-export-to-odt nil nil nil
		       '(:author nil
			 :with-toc 2
			 :headline-levels 2
			 :section-numbers nil))
