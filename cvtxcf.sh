#!/bin/bash

gimp -i --batch-interpreter=python-fu-eval -b - << EOF
import gimpfu

def convert(filename, new_name):
    img = pdb.gimp_file_load(filename, filename)
    layer = pdb.gimp_image_merge_visible_layers(img, 1)

    pdb.gimp_file_save(img, layer, new_name, new_name)
    pdb.gimp_image_delete(img)

convert('$1', '$2')

pdb.gimp_quit(1)
EOF
