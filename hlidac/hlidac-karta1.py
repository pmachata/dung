template = "text.svg"

name = "Hlídač"
blurb = """

<b>Fyzička 5, Finesa 2, Duše 4,</b> Životy 13
<br>Exoskelet: <b>Fyzička +1</b> Životy 4, Zbroj 1
<br>
<br><i>Sekera</i> (Fyzička): 5 (+ 1) + k6, zranění 2
<br><i>Pařát</i> (Fyzička): 5 (+ 1) + k6, zranění 1
<br>
<br>Dokud žije, dává psům výhodu v boji.
<br>Když je "zabit" exoskelet, hlídač přijde o bonus k Fyzičce.

"""
