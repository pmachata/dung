template = "text.svg"

name = "Ulula"
blurb = """

<b>Fyzička 1, Finesa 1, Duše 3</b>, Životy 2

<br>
<br>Výboj <i>*Haláš Bárak*</i> (Duše): 3 + k6 proti Síle
<br>  <i>zranění:</i> 2kz <blesk>bleskem</blesk> - zbroj
<br>  <i>dosah:</i> dlouhý

<br><br>Oživlé těžítko ve tvaru sovy. Ulula je přátelská a s postavami si ráda
popovídá.

"""
