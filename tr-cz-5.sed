# this works with output of tr-cz-1.sed

# Fix weird API from tr-cz-1
s/ᴕ/o_u/g
s/h</ch/g

s/h</ch/g
s/c</ç/g
s/n</ŋ/g
s/r</ŗ/g
s/s</ʂ/g
s/z</ȥ/g

s/b/ʙ/g
s/d/ᴅ/g
s/f/ɾ/g
s/g/ɢ/g
s/h/ʜ/g
s/j/ᴊ/g
s/k/ᴋ/g
s/l/ʟ/g
s/p/ᴘ/g
s/t/ᴛ/g

s/</̨/g
s/\([^_']\)'/'\1/g

s/\([aeiouy]\)_\([aeiouy]\)/_\1\2/g
s/\([^_']\)a/\1̄/g
s/\([^_']\)e/\1̈/g
s/\([^_']\)i/\1̇/g
s/\([^_']\)y/\1̇/g
s/\([^_']\)o/\1̊/g
s/\([^_']\)u/\1̆/g
s/'\(.\)/\1́/g

s/y/ʏ/g
s/i/ɪ/g
s/e/ᴇ/g
s/a/ᴀ/g

s/_//g
s/=//g

s/\./ ⚬/g
s/!/ 🞄/g
