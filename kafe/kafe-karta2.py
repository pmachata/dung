template = "text.svg"
background = "elfi"
pic = "kafe-karta1.png"

name = "Dřevěná krabička"
blurb = """

Zdobená krabička. Uvnitř je tmavě hnědý jemný prášek.

<br><br><i>Černý nápoj z tohoto prášku má sice nevábnou chuť a vůni, ale i
povzbudivé účinky a zahání ospalost.</i>

"""
