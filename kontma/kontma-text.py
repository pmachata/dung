template = "text.svg"
frame = "elfi"
pic = "kontma-mala.png"

name = "Koncentrovaná tma"
blurb = """

Pokročilý alchymistický recept.

<br><br>Když se lahvička s koncentrovanou tmou rozbije nebo otevře, tma se
"vyzáří" do okolí, jako by se jednalo o jasné světlo, ale místo osvětlování
zatmavuje. V kontaktní vzdálenosti od epicentra je naprostá tma, a s
rostoucí vzdáleností intenzita tmy klesá. Tma v oblasti vydrží několik
minut.

"""
