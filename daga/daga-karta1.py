template = "text.svg"
background = "trpaslici"

name = "Dága"
blurb = """

Robustní dýka, která má ze strany hřbetu řadu hlubokých zubů. Za výhodu je možno
těmito zuby zachytit soupeřovu zbraň, pokud je podobná meči, a tím protivníkovi
zabránit v jeho používání. Na osvobození meče se hází Silou proti Síle.

"""
