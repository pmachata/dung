template = "text.svg"

name = "Slizovec"
blurb = """

<b>Fyzička 6, Finesa 2, Duše -</b>, Životy 4
<br>Velikost 8 (250 kg)
<br>Zbroj 0

<br><br>Slizovec se podobá plísni nebo lišejníku a číhá na stropě, aby mohl
spadnout na procházející oběť. Jinak se hraje jako čtvrtina Hnusu plíživého.

"""
