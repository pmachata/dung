template = "text.svg"
background = frame = "elfi"
pic = "protraktor-karta1.png"

name = "Protraktor"
blurb = """

Kulatý kovový objekt čočkovitého tvaru. Z jedné strany je kulaté skleněné
okénko, pod kterým jsou zlatavé ornamenty.

Po vyslovení formule <b>L'galot makor</b> po dobu soustředění ukazuje
pomocí ornamentů toky a ohniska magické energie v okolí. Lze tak například
zjistit, kdo je sesilatelem právě probíhajícího kouzla, nebo že nějaké
kouzlo na daném místě právě probíhá.

"""
