template = "text.svg"

name = "Šejstan"
blurb = """

<b>Fyzička 3, Finesa 1, Duše -</b>, Životy 11
<br>Zbroj 1 (lehká zbroj)

<br>
<br><i>Šavle</i> (Fyzička): 3 + k6, zranění 1
<br><i>Nůž</i> (Finesa): 1 + k6, zranění 1

<br><br>Zombie zloděje Šejstana.

"""
