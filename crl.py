import xml.dom.minidom
import xml.dom
import sys

def inkscape_attr(node, attr):
    return node.getAttributeNS("http://www.inkscape.org/namespaces/inkscape",
                               attr)

def content(fn):
    ret = ""
    doc = xml.dom.minidom.parse(fn)
    for node in doc.documentElement.childNodes:
        if node.nodeType == xml.dom.Node.ELEMENT_NODE and \
           node.tagName == "g" and \
           inkscape_attr(node, "groupmode") == "layer":
            ret += node.toxml()
    return ret

def defs(fn):
    ret = ""
    doc = xml.dom.minidom.parse(fn)
    for node in doc.documentElement.childNodes:
        if node.nodeType == xml.dom.Node.ELEMENT_NODE and \
           node.tagName == "defs":
            for child in node.childNodes:
                if child.nodeType == xml.dom.Node.ELEMENT_NODE:
                    ret += child.toxml()
    return ret

values = {}
for i, fn in enumerate(sys.argv[1:]):
    values["c%d" % i] = content(fn)
    values["d%d" % i] = defs(fn)
else:
    while i < 7:
        i += 1
        values["c%d" % i] = content("templ/empty.svg")
        values["d%d" % i] = ""

templ = sys.stdin.read()
sys.stdout.write(templ.format_map(values))
