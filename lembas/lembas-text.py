template = "text.svg"
background = frame = "transparent"
pic = "lembas-maly.png"

name = "Lembas"
blurb = """

Trvanlivý chléb, která dodá tělu všechny potřebné živiny (nejedná se tedy o
pečivo, ale o kompletní stravu). Malá porce vystačí jako jídlo na jeden
den. Po všech těch letech je poněkud zatuchlý, ale pořád jedlý a stejně
výživný, jako kdykoli předtím.

"""
