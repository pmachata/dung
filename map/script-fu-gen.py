import sys

outfile = sys.argv[1]
if outfile in ("--help", "-h"):
    print("%s <OUTFILE> <FILE> [<FILE>...]" % sys.argv[0])
    sys.exit(0)

print("""
(define (add-layer image filename)
  (let ((layer (car (gimp-file-load-layer RUN-NONINTERACTIVE image filename))))
    (gimp-image-add-layer image layer 0)))

(let ((image (car
	      (gimp-image-new 1 1 RGB))))
%s
  (gimp-image-resize-to-layers image)
  (let ((drawable (car (gimp-image-get-active-drawable image)))
	(filename "%s"))
    (gimp-xcf-save 0 image drawable filename filename)))

(gimp-quit 0)
;(gimp-display-new image)
""" % (("\n".join(
        """  (add-layer image "%s")""" % fn for fn in sys.argv[2:]
       ), sys.argv[1])))
