template = "text.svg"
background = frame = "elfi"
pic = "termoska-mala.png"

name = "Termoska"
blurb = """

Stříbrná nádoba se šroubovacím uzávěrem.

<br><br>Když je připoutaná, udržuje teplotu toho, co obsahuje. Nejnižší teplota,
kterou dokáže dlouhodobě udržet, je -5°C, nejvyšší asi 95°C.

"""
