template = "text.svg"
background = frame = "elfi"
pic = "kotva-mala.png"

name = "Kotva"
blurb = """

Ocelové očko. Když se někdo očka dotýká a pronese slova "ha-chazak chazak",
očko se ukotví. Potom drží na místě, na kterém zrovna je, jako by bylo
přišroubované.

<br><br>Když se někdo očka dotýká a pronese slova "l'šahrer", naopak jej
uvolní.

"""
