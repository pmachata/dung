#!/usr/bin/python3
import sys

rules = [
    ("nth", "am"),
    ("sch", "len"),
    ("scr", "silm"),
    ("shr", "lin"),
    ("spl", "sal"),
    ("spr", "men"),
    ("squ", "na"),
    ("str", "ntr"),
    ("thr", "sul"),
    ("ing", "elb"),
    ("gua", "rea"),
    ("an", "on"),
    ("bl", "tean"),
    ("br", "bum"),
    ("ch", "nm"),
    ("ck", "sin"),
    ("cl", "sl"),
    ("cr", "ber"),
    ("dr", "lor"),
    ("er", "ier"),
    ("en", "an"),
    ("fl", "p"),
    ("fr", "f"),
    ("gh", "nom"),
    ("gl", "gal"),
    ("gr", "s"),
    ("he", "nja"),
    ("in", "elen"),
    ("nd", "g"),
    ("ng", "d"),
    ("ou", "á"),
    ("on", "en"),
    ("ph", "d"),
    ("pl", "pel"),
    ("pr", "r"),
    ("qu", "ke"),
    ("re", "sa"),
    ("sc", "gal"),
    ("sh", "bm"),
    ("sk", "sol"),
    ("sl", "kas"),
    ("sm", "tr"),
    ("sn", "zm"),
    ("sp", "kn"),
    ("st", "k"),
    ("sw", "saen"),
    ("th", "c"),
    ("tr", "dl"),
    ("tw", "bl"),
    ("wh", "t"),
    ("wr", "dol"),
    ("ea", "á"),
    ("ai", "dea"),
    ("au", "oa"),
    ("aw", "in"),
    ("ay", "í"),
    ("kea", "ká"),
    ("ei", "ó"),
    ("eu", "é"),
    ("ew", "en"),
    ("ey", "ea"),
    ("ie", "oa"),
    ("oi", "o"),
    ("ou", "i"),
    ("ow", "ol"),
    ("oy", "ae"),
    ("aa", "a"),
    ("bb", "b"),
    ("cc", "c"),
    ("dd", "g"),
    ("ee", "e"),
    ("ff", "d"),
    ("gg", "f"),
    ("hh", "cí"),
    ("ii", "i"),
    ("jj", "k"),
    ("kk", "j"),
    ("ll", "m"),
    ("mm", "n"),
    ("nn", "p"),
    ("oo", "ó"),
    ("pp", "n"),
    ("qq", "k"),
    ("rr", "s"),
    ("ss", "t"),
    ("tt", "r"),
    ("uu", "u"),
    ("vv", "l"),
    ("ww", "v"),
    ("xx", "k"),
    ("yy", "i"),
    ("zz", "r"),
    ("a", "á"),
    ("b", "n"),
    ("c", "k"),
    ("d", "m"),
    ("e", "é"),
    ("f", "l"),
    ("g", "h"),
    ("h", "c"),
    ("i", "a"),
    ("j", "r"),
    ("k", "g"),
    ("l", "z"),
    ("m", "n"),
    ("n", "l"),
    ("o", "e"),
    ("p", "t"),
    ("q", "k"),
    ("r", "s"),
    ("s", "t"),
    ("t", "r"),
    ("u", "o"),
    ("v", "p"),
    ("w", "v"),
    ("x", "k"),
    ("y", "í"),
    ("z", "d"),
]

# elvish -> plain
rrules = {}

for plain, elvish in rules:
    rrules.setdefault(elvish, []).append(plain)

def plain_to_elvish():
    while True:
        line = sys.stdin.readline().strip("\n")
        if line == "":
            break

        out = ""
        while line != "":
            for src, dst in rules:
                if line.startswith(src):
                    out += dst
                    line = line[len(src):]
                    break
            else:
                print("Couldn't match %s" % line)
                sys.exit(1)
        print(out)

def elvish_to_plain():
    while True:
        line = sys.stdin.readline().strip("\n")
        if line == "":
            break

        outs = [("", line)]
        while True:
            new_outs = []
            for out, line in outs:
                if len(line) == 0:
                    new_outs.append((out, line))
                    continue
                for elvish, plains in rrules.items():
                    if line.startswith(elvish):
                        new_line = line[len(elvish):]
                        for plain in plains:
                            new_outs.append((out + plain, new_line))
            if new_outs == outs:
                break
            outs = new_outs

        for out in sorted((o for o, l in outs if len(line) == 0),
                          key=len)[::-1]:
            print(out)

if "unelvish" in sys.argv[0]:
    elvish_to_plain()
else:
    plain_to_elvish()
