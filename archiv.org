* co
  - sabotér?
  - rozšiřování rafinerie
  - instrukce pro stavbyvedoucího
    - popis konstrukce tajných dveří, umístění zámku a podobné detaily
    - dále popis přehrazení Aonovy kapsy
      - ta by mohla být pod úrovní podlahy, proto pro ni neměli využití
  - stížnosti na nefunkční portál

* Zadání vyhotovení tajných dveří
  - xxx nebude se pouzivat, Aon asi nakonec bude nahore v mistnosti s
    ventilatory

K rukám váženého artifika Rákena

Co se týče větrací šachty přilehlé k laboratoři. Bezpečnostní profil této šachty
je nevyhovující, proto je třeba ji zazdít. Větrat touto šachtou zapotřebí
nebude, k dispozici laboratoře bude samočinný odsavač s filtrováním.

---

K rukám velmistra Namiera

Předpis stanoví zřizovat nové alchemické laboratoře s přímým přístupem čerstvého
vzduchu. Šachtu nelze zazdít.

---

K rukám váženého artifika Rákena

Který předpis? Ze zakládacího ustanovení vyplývá nutnost dvouúrovňového
perimetru. Nelze mít laboratoř přístupnou bez překonání dvou zamykatelných dveří
a stráže. V šachtě žádné dveře nejsou!

---

K rukám velmistra Namiera

Předpis, o kterém mluvím, je edykt Magistratury o věcech alchemických.
Co se přístupnosti týče, v šachtě je mříž!

---

K rukám váženého artifika Rákena

Pane, mříž není dveřmi. Prostudoval jsem dotyčný edykt, a věřím, že jeho smyslem
je zajistit pracovníkům laboratoře vzduch prostý škodlivin. Připomínám, že tuto
roli bude zastávat samočinná odsávací soustava.

Leč budiž, ponechte v zazděné stěně malý větrací otvor, ne větší dlaně. Věřím,
že tím je formálně splněna podmínka mít v laboratoři přístup čerstvého vzduchu,
jakož i naplněn smysl zakládacího ustanovení.
