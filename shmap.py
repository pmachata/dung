import xml.dom.minidom
import xml.dom
import sys

show_layers = set(sys.argv[1:])
found_layers = set()

def inkscape_attr(node, attr):
    return node.getAttributeNS("http://www.inkscape.org/namespaces/inkscape",
                               attr)

doc = xml.dom.minidom.parse(sys.stdin)
for node in doc.documentElement.childNodes:
    if node.nodeType == xml.dom.Node.ELEMENT_NODE and \
       node.tagName == "g" and \
       inkscape_attr(node, "groupmode") == "layer":
        styles_str0 = node.getAttribute("style")
        styles = list(item.split(":") for item in styles_str0.split(";"))
        layer_id = node.getAttribute("id")
        found_layers.add(layer_id)

        display = "inline" if layer_id in show_layers else "none"
        for i, (style, value) in enumerate(styles):
            if style == "display":
                styles[i] = (style, display)
                break
        else:
            styles.append(("display", display))

        styles_str = ";".join("%s:%s" % (style, value)
                              for style, value in styles)
        if styles_str != styles_str0:
            sys.stderr.write("%s:\t%s -> %s\n" % (layer_id, styles_str0,
                                                  styles_str))
            node.setAttribute("style", styles_str)

not_found = None
for not_found in show_layers - found_layers:
    sys.stderr.write("error: Layer not found: %s\n" % not_found)
if not_found:
    exit(1)

print(doc.toxml())
