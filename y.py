from collections import Counter

def counter_for_prefix(s, pfx):
    ctr = Counter()
    s0 = ()
    for c in s:
        if s0 == pfx:
            ctr[s0 + (c,)] += 1
        s0 += c,
        if len(s0) > len(pfx):
            s0 = s0[1:]

    for s0,n in ctr.most_common():
        if n >= 5:
            ctr2 = counter_for_prefix(s, s0)
            for s1,n1 in ctr2.items():
                if n1 >= 2:
                    ctr[s0] -= n1
                    ctr[s1] += n1

    for s0,n in list(ctr.items()):
        if n == 0:
            del ctr[s0]

    return ctr

def analyze(s, ctr):
    ret = []
    while s != "":
        for s0,n in ctr.most_common():
            if s.startswith(s0):
                ret.append(s0)
                s = s[len(s0):]
                break
    return ret

def find_subseq(haystack, needle):
    for i, _ in enumerate(haystack):
        ok = True
        for j, n_e in enumerate(needle):
            try:
                h_e = haystack[i + j]
            except IndexError:
                h_e = None
                ok = False
            if n_e != h_e:
                ok = False
        if ok:
            return i
    return -1

def analyze(s, ctr):
    for s0 in sorted(ctr.keys(), key=len, reverse=True):
        idx = find_subseq(s, s0)
        if idx >= 0:
            a = analyze(s[:idx], ctr)
            b = analyze(s[idx + len(s0):], ctr)
            return a + [s0] + b
    return []

if __name__ == "__main__":
    s="DOUGHBOYWASANINFORMALTERMFORAMEMBEROFTHEUNITEDSTATESARMYORMARINECORPSITWASUSEDASSEARLYASTHEMEXICANAMERICANWAROF18461848BUTTODAYITMAINLYREFERSTOMEMBEROFTHEAMERICANEXPEDITIONARYFORCESINWORLDWARI"

    ctr = counter_for_prefix(s, "")
    print(ctr)
    print(analyze(s, ctr))
