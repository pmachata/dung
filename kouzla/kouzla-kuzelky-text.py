template = "text.svg"
background = frame = "elfi"
pic = "svitek.png"

name = "Kuželky"
blurb = """

Svitek s pokročilým kouzlem.

<br><br><b>*Kadoret*</b> Dosah: krátký; Rozsah: kontaktní vzdálenost od
cíle; Útok: Duše + k6 proti Fyzičce; Zranění: 4kz <drceni>drcením</drceni>

<br><br>Z kouzelníkových rukou vylétne malá kulička, a v místě cíle
vybuchne tlakovou vlnou, která smete všechny, kteří jsou kolem.


"""
