template = "text.svg"
background = frame = "elfi"
pic = "svitek.png"

name = "Odvolací rituál"
blurb = """

Svitek s pokročilým rituálem.

<br><br>Svitek obsahuje rituál, který má <b>odvolat</b> určitého démona.
Jeho jméno je však na svitku vynecháno a místo něj je napsáno jen X.


"""
