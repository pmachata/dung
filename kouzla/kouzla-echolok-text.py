template = "text.svg"
frame = "elfi"
pic = "kouzla-echolok-mala.png"

name = "Echolokace"
blurb = """

Svitek se začátečnickým kouzlem.

<br><br><b>*Mikum héd*</b> Dosah: sesilatel; Trvání: několik minut nebo do
ukončení

<br><br>Sesilatel nahradí svůj zrak smyslem, s jehož pomocí vidí předměty,
které jsou hmotné, podobně ultrasluchu. Mimo to vnímá jiné blízké uživatele
ultrasluchu (netopýry, krolly, sesilatele Echolokace).

"""
