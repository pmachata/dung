template = "text.svg"
background = frame = "elfi"
pic = "koflak-maly.png"

name = "Kovový flakon"
blurb = """

Kovový flakon s fialovou tekutinou.

<br><br>Tekutina je žíravá, při dotyku pálí. Způsobuje zranění 3kz
<jed>žíravinou</jed> každé kolo, kdy je oběť působení vystavena.

"""
