template = "text.svg"
background = frame = "elfi"

name = "Megacloumák"
blurb = """

Lektvar.

<br><br><i>Trvání:</i> Několik minut.

<br><br>Odstraní mdloby, ochromení, paralýzu a křeče a po dobu trvání tě probere
z bezvědomí.

"""
