template = "text.svg"
background = frame = "elfi"
pic = "zapalovac-karta1.png"

name = "Zapalovač"
blurb = """

Tmavě červená krychle s kováním velikosti kudúčí pěsti. Po připoutání a
odklopení víčka na horní straně lze objekt aktivovat, aby z něj vyšlehl
plamínek. Ten bude hořet, dokud bude předmět připoután a aktivován.

"""
