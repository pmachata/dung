template = "text.svg"

name = "Hlídačův pes"
blurb = """

<b>Fyzička 2, Finesa 3, Duše 1</b>, Životy 6
<br>Zbroj 2 (železné tělo)

<br>
<br><i>Oheň</i> (Finesa): 3 (+1 Hlídač) + k6, zranění 4kz <ohen>ohňem</ohen>

<br><br>Psi původně byli laboratorními kahany a podobným vybavením. Ve
skutečnosti se podobají spíš nějakým salamandrům a sálá z nich horko.

"""
