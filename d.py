import sys
from collections import Counter
from c import split_to_letters

ctr = Counter()
for line in sys.stdin.readlines():
    line = line.strip()
    letters = split_to_letters(line)
    ctr += Counter(letters)

print("%d letters" % len(ctr))

least_common = ctr.most_common()[::-1]
print("least freqent %s" % str(least_common))
