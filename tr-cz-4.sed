# this works with output of tr-cz-1.sed

s/c</ç/g
s/d</ᴆ/g
s/h</χ/g
s/n</ŋ/g
s/r</ŗ/g
s/s</ş/g
s/t</τ/g
s/z</ᴣ/g

s/b/ʙ/g
s/d/ᴅ/g
s/f/ɾ/g
s/g/ɢ/g
s/h/ʜ/g
s/j/ᴊ/g
s/k/ᴋ/g
s/l/ʟ/g
s/p/ᴘ/g
s/t/ᴛ/g

s/\([^_']\)</\1̭/g
s/\([^_']\)'/'\1/g

s/\([^_']\)a/\1̄/g
s/\([^_']\)e/\1̈/g
s/\([^_']\)i/\1̇/g
s/\([^_']\)y/\1̇/g
s/\([^_']\)o/\1̊/g
s/\([^_']\)u/\1̆/g
s/'\(.\)/\1́/g

s/y/ʏ/g
s/i/ɪ/g
s/e/ᴇ/g
s/a/ᴀ/g

s/_//g
s/=//g

s/\./ ⚬/g
s/!/ 🞄/g
