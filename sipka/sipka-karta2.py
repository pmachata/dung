template = "text.svg"
background = frame = "elfi"
pic = "sipka-karta1.png"

name = "Chytrá šipka"
blurb = """

Šipka do kuše.

<br><br>Pokud je střelec při vypouštění šipky vyřkne slova <b>*chec
jašir*</b>, může její let částečně ovlivnit, což mu dá při střelbě výhodu.

"""
