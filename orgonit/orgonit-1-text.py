template = "text.svg"
background = frame = "elfi"
pic = "orgonit-1-mala.png"

name = "Orgonit"
blurb = """

Pyramida z Orgonitu s runou na jedné straně.

<br><br>Při přípravě alchymistického lektvaru zastupuje jednu kapku esence,
a stane se pak součástí výrobku. Po spotřebování předmětu, nebo poté, co
již není potřeba, je možné článek znovu vyjmout, a použít znovu na něco
jiného.

"""
