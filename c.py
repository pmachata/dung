from unicodedata import combining

def split_to_letters(s):
    ret = []
    for c in s:
        if combining(c):
            ret[-1] += c
        else:
            ret.append(c)
    return ret
