from collections import Counter
from pygraphviz import *
from unicodedata import combining
import string
import sys
import y
from c import split_to_letters

if len(sys.argv) != 2 or sys.argv[1] in ("-h", "--help"):
    print(sys.argv[0], "<outfile>")
    sys.exit()
else:
    outfile = sys.argv[1]

text = sys.stdin.read().strip()
s0 = list(split_to_letters(w) for w in text.split())
s1 = sum(s0, [])

ctr = y.counter_for_prefix(s1, ())
s0 = list(y.analyze(w, ctr) for w in s0)

s2 = []
for c in sum(s0, []):
    if c not in s2:
        s2.append(c)

def diameter(i):
    return 0.3 + 0.2 * i

nodectr = Counter()
for s in s0:
    nodectr += Counter(s)

g = ""
g += """
graph "" {
    graph [overlap="false", splines="true"]
    node [shape="circle"]
"""

for c in set(s2):
    r = diameter(nodectr[c])
    g += """    "%s" [width="%s", height="%s",
                    shape="plaintext",
                    fontname="PragmataPro"]\n""" \
                            % ("".join(c), str(r), str(r))

key = 0
last_c = None
for s in s0:
    style = "dashed"
    for c in s:
        if last_c is not None:
            g += """    "%s" -- "%s" [key="%d", style="%s"]\n""" \
                                            % ("".join(last_c),
                                                "".join(c), key, style)
            key += 1
        last_c = c
        style = "solid"
g += "}\n"

g0 = AGraph(g, directed=False, overlap=False, splines=True)
if False:
    g0.layout(prog="dot")
else:
    g0.layout(prog="twopi")

if not True:
    g0.write()
    sys.exit(0)

nodectr = Counter()
style = "bold"
for s in s0:
    for c in s:
        nodectr.update((c,))
        nodei = nodectr[c]
        if nodei > 0:
            noden = "".join(c) + str(nodei)
            r = diameter(nodei)
            pos = g0.get_node("".join(c)).attr["pos"]
            g0.add_node(noden, label="", width=str(r), height=str(r), pos=pos,
                        style=style)
            style = "solid"

def parse_pos(pos):
    return tuple(float(c) for c in pos.split(","))
def fmt_pos(pos):
    return ",".join(str(c) for c in pos)
def parse_poslist(poslist):
    return list(parse_pos(pos) for pos in poslist.split())
def fmt_poslist(poslist):
    return " ".join(fmt_pos(pos) for pos in poslist)
def d_pos(pos1, pos2):
    x1, y1 = pos1
    x2, y2 = pos2
    return (x2 - x1, y2 - y1)

def adjust_pos(nodei, pos, node):
    npos = parse_pos(node.attr["pos"])
    d = d_pos(pos, npos)
    l = (d[0] ** 2 + d[1] ** 2) ** 0.5
    dd = tuple(c / l for c in d)
    r = diameter(nodei) * 72 / 2 # inches -> points, diameter -> radius
    return tuple(c - dc*r for c, dc in zip(npos, dd))

edges = {}
for a, b, key in g0.edges(keys=True):
    e = g0.get_edge(a, b, key)
    edges.setdefault((a, b), []).append(e)

nodectr = Counter()
last_c = None
last_nodei = None
for s in s0:
    for c in s:
        nodectr.update((c,))
        nodei = nodectr[c]
        if last_c is not None:
            e = edges["".join(last_c), "".join(c)].pop()
            e_poslist = parse_poslist(e.attr["pos"])

            na = g0.get_node("".join(last_c))
            e_poslist[0] = adjust_pos(last_nodei, e_poslist[0], na)

            nb = g0.get_node("".join(c))
            e_poslist[-1] = adjust_pos(nodei, e_poslist[-1], nb)

            e.attr["pos"] = fmt_poslist(e_poslist)

        last_c = c
        last_nodei = nodei

if False:
    nodectr = Counter()
    for a, b, key in g0.edges(keys=True):
        e = g0.get_edge(a, b, key)
        e_poslist = parse_poslist(e.attr["pos"])

        na = g0.get_node(a)
        e_poslist[0] = adjust_pos(nodectr, e_poslist[0], na)

        nb = g0.get_node(b)
        nodectr.update((b,))
        e_poslist[-1] = adjust_pos(nodectr, e_poslist[-1], nb)

        x, y = e_poslist[0]
        e_poslist[0] = (x + 10, y)
        e.attr["pos"] = fmt_poslist(e_poslist)

#g1.layout(prog="twopi")
g0.draw(outfile)
