template = "text.svg"
background = frame = "elfi"
pic = "kamtich-mala.png"

name = "Kámen ticha"
blurb = """

Kamenná kostka o hraně asi 12 cm a váze 5 kg, s runou na jedné straně.

<br><br>Po vyslovení formule *Šeket nešef* rozvine kolem kamene sféru ticha
o průměru až pět metrů, skrze kterou žádné zvuky neproniknou tam ani zpět.
Stěna se pohybuje spolu s kamenem a vydrží několik minut.

"""
