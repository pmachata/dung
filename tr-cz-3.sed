# this works with output of tr-cz-1.sed

s/y/i/g
s/ᴕ/o_u/g

s/b/ʙ/g
s/d/ᴅ/g
s/f/ɾ/g
s/g/ɢ/g
s/h/ʜ/g
s/j/ᴊ/g
s/k/ᴋ/g
s/l/ʟ/g
s/p/ᴘ/g
s/t/ᴛ/g

s/\(.\)</<\1/g

s/<\([^=]\)/\1̭/g

s/\([^=]\)a/\1̂/g
s/\([^=]\)e/\1̈/g
s/\([^=]\)i/\1̍/g
s/\([^=]\)o/\1̊/g
s/\([^=]\)u/\1̆/g
s/\(.\)'/\1́/g

s/_̊/o/g
s/_̆/ᴜ/g
s/_̂/ʌ/g
s/_̍/ɪ/g
s/_̈/ᴇ/g
s/̍́/́/g
s/̈́/̋/g

s/=//g
