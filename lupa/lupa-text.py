template = "text.svg"
frame = "elfi"
pic = "lupa-mala.png"

name = "Introspekční lupa"
blurb = """

Po vyslovení formule <b>L'galot makor</b> je po dobu soustředění skrze lupu
vidět kouzla a magická ohniska aktivní v krátké vzdálenosti.

"""
