import os
import sys
import importlib
import re
import xml.dom.minidom
import xml.dom

def svgify_blurb(blurb):
    blurb = re.sub("<!--.*?-->", "", blurb)
    return (("""<p>""" + blurb + "</p>")
            .replace("<br>", "</p><p>")
            .replace("<b>", """<flowSpan style="font-weight:bold">""")
            .replace("</b>", """</flowSpan>""")
            .replace("<i>", """<flowSpan style="font-style:italic">""")
            .replace("</i>", """</flowSpan>""")
            .replace("<p>", """<flowPara style="fill:#323232">""")
            .replace("</p>", """</flowPara>""")

            .replace("<ohen>",     """<flowSpan style="fill:#f26522">""")
            .replace("<mraz>",     """<flowSpan style="fill:#31c5f4">""")
            .replace("<blesk>",    """<flowSpan style="fill:#687bbc">""")
            .replace("<jed>",      """<flowSpan style="fill:#8aa73d">""")
            .replace("<ziravina>", """<flowSpan style="fill:#c3b346">""")
            .replace("<sek>",      """<flowSpan style="fill:#a691aa">""")
            .replace("<bod>",      """<flowSpan style="fill:#c4842b">""")
            .replace("<drceni>",   """<flowSpan style="fill:#515245">""")
            .replace("<dusevni>",  """<flowSpan style="fill:#28baa8">""")

            .replace("</ohen>",    """</flowSpan>""")
            .replace("</mraz>",    """</flowSpan>""")
            .replace("</blesk>",   """</flowSpan>""")
            .replace("</jed>",     """</flowSpan>""")
            .replace("</ziravina>","""</flowSpan>""")
            .replace("</sek>",     """</flowSpan>""")
            .replace("</sek>",     """</flowSpan>""")
            .replace("</drceni>",  """</flowSpan>""")
            .replace("</dusevni>", """</flowSpan>""")
    )

path = sys.argv[1]
if path in ("-h", "--help"):
    print("crk.py vyrabi karty z sablon")
    print("pouziti: crk.py <adresar>/<modul>.py")
    print("<adresar>/<modul>.py je soubor v Pythonu s informacemi o tom,")
    print("jak kartu vytvorit (z jake sablony, jake paramtery)")
    sys.exit(0)

if path in ("--deps",):
    show_deps = True
    path = sys.argv[2]
else:
    show_deps = False

assert path.endswith(".py")
path = path[:-3]
path = path.replace("/", ".", 1)
assert "/" not in path
rel, name = path.split(".")

mod = importlib.import_module(path)

values = {
    "root": os.getcwd(),
    "rel": rel,
    "uprel": "..",
}

def card_n_dispatch(value):
    if type(value) is tuple:
        match, = re.finditer("\d+$", path)
        value = value[int(match[0]) - 1]
    return value

try:
    values["background"] = mod.background
except:
    values["background"] = "transparent"

try:
    values["unique"] = mod.unique
except:
    pass

try:
    title = mod.name
except:
    title = ""
title = card_n_dispatch(title)
values["name"] = title

try:
    values["pic"] = mod.pic
except:
    values["pic"] = "%s.png" % name

try:
    values["foreground"] = mod.foreground
except:
    pass

try:
    values["frame"] = mod.frame
except:
    values["frame"] = "transparent"

try:
    blurb = mod.blurb
except:
    blurb = ""
values["blurb"] = svgify_blurb(card_n_dispatch(blurb))

template = "templ/" + mod.template

data = open(template).read()
svg = data.format_map(values)

if not show_deps:
    sys.stdout.write(svg)
    sys.exit(0)

def sodipodi_attr(node, attr):
    return node.getAttributeNS("http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd",
                               attr)

doc = xml.dom.minidom.parseString(svg)
for node in doc.documentElement.getElementsByTagName("image"):
    print(sodipodi_attr(node, "absref"))

print(template)
