template = "text.svg"
background = "elfi"
pic = "tableta-mala.png"

name = "Tableta"
blurb = """

Po uplynutí několika minut latenční doby je uživatel asi hodinu schopen
mimořádně dobře se soustředit. To mu dává výhodu ve všech hodech na Duši.
Další hodinu je však naopak roztěkaný, a má naopak na hody na Duši
nevýhodu.

<br><br>Pozitivní efekt lze prodlužovat požitím dalších tablet, ovšem potom
stejně tak roste i doba negativního efektu.

"""
