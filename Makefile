TR1 = tr-cz-1.sed
TR2 = tr-cz-5.sed

DVERE = \
	dvere/dvere-satna-karta1 \
	dvere/dvere-laborator-karta1 \
	dvere/dvere-kotelna-karta1 \
	dvere/dvere-kantyna-karta1 \
	dvere/dvere-kuchyne-karta1 \
	dvere/dvere-umyvarna-karta1 \
	dvere/dvere-spravkyne-karta1 \
	dvere/dvere-velmistr-karta1 \
	dvere/dvere-tavirna-karta1 \
	dvere/dvere-pracovny-karta1 \
	dvere/dvere-knihovna-karta1

CARDS = \
	astra/astra-obrazek \
	astra/astra-zuby-obrazek \
	astra/astra-zuby-text \
	demizon/demizon-cerny-obrazek \
	demizon/demizon-cerny-text \
	demizon/demizon-fialovy-obrazek \
	demizon/demizon-fialovy-text \
	demizon/demizon-sedy-obrazek \
	demizon/demizon-sedy-text \
	demizon/demizon-zluty-obrazek \
	demizon/demizon-zluty-text \
	$(DVERE) \
	galad/galad-karta1 \
	galad/galad-karta2 \
	hadi/hadi-karta1 \
	hadi/hadi-karta2 \
	hlidac/hlidac-karta1 \
	hlidac/hlidac-karta2 \
	insomknof/insomknof-karta1 \
	insomknof/insomknof-karta2 \
	insomknof/insomknof-karta3 \
	instawand/instawand-obrazek \
	instawand/instawand-text \
	kamtich/kamtich-obrazek \
	kamtich/kamtich-text \
	klic/klic-d-nadpis \
	klic/klic-d-text \
	klic/klic-n-nadpis \
	klic/klic-n-text \
	klic/klic-p-nadpis \
	klic/klic-p-text \
	klic/klic-q-nadpis \
	klic/klic-q-text \
	klic/klic-maly-nadpis \
	klic/klic-maly-text \
	klic/klic-mosazny-nadpis \
	klic/klic-mosazny-text \
	klic/klic-spaleny-nadpis \
	klic/klic-spaleny-text \
	koflak/koflak-obrazek \
	koflak/koflak-text \
	kontma/kontma-text \
	kotva/kotva-obrazek \
	kotva/kotva-text \
	kouzla/kouzla-echolok-text \
	kouzla/kouzla-kuzelky-text \
	kouzla/kouzla-odvorit-text \
	krystal/krystal-nadpis \
	krystal/krystal-obrazek \
	lampa/lampa-nesvit-obrazek \
	lampa/lampa-svit-obrazek \
	lampa/lampa-svit-text \
	lembas/lembas-obrazek \
	lembas/lembas-text \
	lupa/lupa-obrazek \
	lupa/lupa-text \
	mandakam/mandakam-obrazek \
	mandakam/mandakam-text \
	megacloumak/megacloumak-karta1 \
	mesic/mesic-karta1 \
	mesic/mesic-karta2 \
	mesic/mesic-karta3 \
	mesic/mesic-keyhole-karta2 \
	mezuza/mezuza-obrazek \
	mezuza/mezuza-nadpis \
	nogos/nogos-1-karta1 \
	nogos/nogos-1-karta2 \
	nogos/nogos-2-karta1 \
	nogos/nogos-2-karta2 \
	nogos/nogos-3-karta1 \
	nogos/nogos-3-karta2 \
	okular/okular-obrazek \
	okular/okular-text \
	orgonit/orgonit-1-obrazek \
	orgonit/orgonit-1-text \
	pes/pes-karta1 \
	pes/pes-karta2 \
	pes/pes-mrtvy-nadpis \
	pes/pes-mrtvy-obrazek \
	prsten/prsten-sigarda-karta1 \
	prsten/prsten-recone-karta1 \
	pulprase/pulprase-karta1 \
	pulprase/pulprase-karta2 \
	senerlag/senerlag-text \
	senerlag/senerlag-obrazek \
	sipka/sipka-karta1 \
	sipka/sipka-karta2 \
	slizovec/slizovec-karta1 \
	slizovec/slizovec-karta2 \
	stribrnit/stribrnit-karta1 \
	tableta/tableta-obrazek \
	tableta/tableta-text \
	termoska/termoska-obrazek \
	termoska/termoska-text \
	ulula/ulula-karta1 \
	ulula/ulula-karta2 \
	zombie/zombie-deloa-karta1 \
	zombie/zombie-deloa-karta2 \
	zombie/zombie-recone-karta1 \
	zombie/zombie-recone-karta2 \
	zombie/zombie-sejstan-karta1 \
	zombie/zombie-sejstan-karta2 \
	zombie/zombie-konstantin-karta1 \
	zombie/zombie-konstantin-karta2 \

MAPS = L0 G0 L1 G1 L2I G2I L2II G2II L2III G2III L2IV G2IV L3 narys okoli

L0-layers = grid teren L0
G0-layers = $(L0-layers) legenda G0
L1-layers = grid teren L1
G1-layers = $(L1-layers) legenda G1
L2I-layers = grid teren legenda L2common L2I
G2I-layers = $(L2I-layers) G2common G2I
L2II-layers = grid teren legenda L2common L2II
G2II-layers = $(L2II-layers) G2common G2II
L2III-layers = grid teren legenda L2common L2III
G2III-layers = $(L2III-layers) G2common G2III
L2IV-layers = grid teren legenda L2common L2IV
G2IV-layers = $(L2IV-layers) G2common G2IV
L3-layers = grid teren legenda L3
narys-layers = narys
okoli-layers = teren

XCF_MAPS = L0 L1 L2 L3

XCF_L0-images = release/map-L0.png map/fog-of-war.png
XCF_L1-images = release/map-L1.png map/fog-of-war.png
XCF_L2-images = release/map-L2IV.png release/map-L2III.png \
		release/map-L2II.png release/map-L2I.png map/fog-of-war.png
XCF_L3-images = release/map-L3.png map/fog-of-war.png

obrazec-lab-layers = poslapani jmeno nodes
obrazec-aon-layers = chyba nodes

# Tento cil zamerne neobsahuje aon/aon.svg, protoze tam byly rucne udelany
# zmeny.
all: cardpages cards maps \
	release/popis-a4.pdf release/popis.odt release/komplet.pdf \
	release/sigarda.pdf \
	release/speech.wav \
	release/namier-stranka.pdf \
	release/obrazce.pdf \
	release/deloa-mapa.pdf

maps: $(MAPS:%=release/map-%.png) $(XCF_MAPS:%=release/map-%.xcf) release/mapy.pdf
rerelease:
	$(MAKE) clean-release
	$(MAKE) all

karty:
	mkdir $@
karty/karty-%.svg: s = $(shell echo $$((($* - 1) * 8 + 1)))
karty/karty-%.svg: e = $(shell echo $$(($s + 7)))
karty/karty-%.svg: karty = $(patsubst %,%.svg,$(wordlist $s,$e,$(CARDS)))
.SECONDEXPANSION:
karty/karty-%.svg: templ/karty2.svg $$(karty) crl.py | karty
	echo $^
	cat $< | python3 crl.py $(karty) > $@ || rm -f $@

NCARDPAGES = $(shell echo $$((($(words $(CARDS)) + 7) / 8)))
CARDPAGES = $(shell seq -f 'karty-%.0f.png' $(NCARDPAGES))
cardpages: $(CARDPAGES:%=release/archy/%) release/archy/karty.pdf
release/archy:
	mkdir $@
release/archy/karty-%.png: karty/karty-%.svg | release/archy
	inkscape $< --export-type=png --export-filename=$@ --export-area-page --export-dpi=300
karty/karty-%.pdf: karty/karty-%.svg | release/archy
	inkscape $< --export-type=pdf --export-filename=$@
release/archy/karty.pdf: $(CARDPAGES:karty-%.png=karty/karty-%.pdf) | release/archy
	pdfunite $^ $@

CARDFILES = $(patsubst %,release/karty/%.png,$(notdir $(CARDS)))
cards: $(CARDFILES)
release/karty:
	mkdir $@
release/karty/%.png: dep = $(firstword $(subst -, ,$(*)))/$(*).svg
release/karty/%.png: $$(dep) | release/karty
	inkscape $< --export-type=png --export-filename=$@ --export-area-page --export-dpi=300

# For some odd reason, $* below doesn't reference the directory part of the
# stem. Construct explicitly as $(@D)/$(*F).
%.svg: deps = $(shell python3 crk.py --deps $(@D)/$(*F).py 2>/dev/null ||:)
%.svg: %.py $$(deps)
	python3 crk.py $< > $@ || rm -f $@
dvere/dvere-%-karta1.png:
	@:
dvere/hinty.org: $(DVERE:%=%.py)
	python3 dvere/gen-hinty.py ${^:dvere/%=%} > $@
.PRECIOUS: $(CARDS:%=%.svg)

map/map-%.svg: map/map.svg
	cat $< | python3 shmap.py $($*-layers) > $@ || rm -f $@
map/map-%.png: map/map-%.svg
	inkscape $< --export-type=png --export-filename=$@ \
		    --export-area-page --export-dpi=300
map/map-okoli.png: map/map-okoli.svg
	inkscape $< --export-type=png --export-filename=$@ \
		    --export-area-drawing --export-dpi=300
map/map90-%.py:
	echo -ne 'template = "map90.svg"\npic = "map-$*.png"\n' > $@
map/map90-%.svg: map/map90-%.py map/map-%.png templ/map90.svg
	python3 crk.py $< > $@ || rm -f $@
map/map90-%.pdf: map/map90-%.svg
	inkscape $< --export-type=pdf --export-filename=$@ \
		    --export-area-page --export-dpi=300

release/map-%.png: map/map-%.png
	cp $< $@
release/map-%.xcf: deps = $(XCF_$(*)-images)
release/map-%.xcf: $$(deps)
	python3 map/script-fu-gen.py $@ $^ | gimp --no-interface -b -
release/mapy.pdf: $(MAPS:%=map/map90-%.pdf)
	pdfunite $^ $@
release/deloa-mapa.pdf: deloa/deloa-mapa.svg
	inkscape $< --export-type=pdf --export-filename=$@ \
		    --export-area-page --export-dpi=300

popis.tex: popis.org dvere/hinty.org
	emacs --batch -f package-initialize --visit $< \
	      --eval '(org-latex-export-to-latex nil nil nil t)'
release/popis.odt: popis.org dvere/hinty.org popis-odt.el
	emacs --batch -f package-initialize --visit $< -l popis-odt.el
	mv popis.odt $@ || rm -f popis.odt
release/popis-%.pdf: popis-%.tex popis.tex
	pdflatex -output-directory release $<
	pdflatex -output-directory release $< # Second run for TOC
release/komplet.pdf: release/popis-a4.pdf \
		     release/sigarda.pdf \
		     release/namier-stranka.pdf \
		     release/deloa-mapa.pdf \
		     release/obrazce.pdf \
		     release/archy/karty.pdf \
		     release/mapy.pdf
	pdfunite $^ $@

sigarda/sigarda-chajda-mapa.png: sigarda/chajda-mapa.xcf
	./cvtxcf.sh $< $@
sigarda/sigarda90-chajda-mapa.py:
	echo -ne 'template = "map90.svg"\npic = "sigarda-chajda-mapa.png"\n' > $@
sigarda/sigarda90-chajda-mapa.pdf: sigarda/sigarda90-chajda-mapa.svg \
				   sigarda/sigarda-chajda-mapa.png
	inkscape $< --export-type=pdf --export-filename=$@ \
		    --export-area-page --export-dpi=300

release/namier-stranka.pdf: namier/namier-stranka.pdf
	cp $< $@
release/sigarda.pdf: sigarda/sigarda90-chajda-mapa.pdf \
		     sigarda/sigarda-denik-strana-1.pdf \
		     sigarda/sigarda-denik-strana-2.pdf \
		     sigarda/sigarda-denik-strana-3.pdf
	pdfunite $^ $@
sigarda/%.pdf: sigarda/%.svg
	inkscape $< --export-type=pdf --export-filename=$@
namier/namier-stranka.pdf: namier/namier-stranka.svg
	inkscape $< --export-type=pdf --export-filename=$@

release/speech.wav: speech/speech.wav
	cp $< $@

aon/obrazec-%.svg: aon/aon.svg
	cat $< | python3 shmap.py $(obrazec-$*-layers) > $@ || rm -f $@
aon/obrazec-%.png: aon/obrazec-%.svg
	inkscape -z --file=$< --export-png=$@ --export-area-page --export-dpi=300
aon/obrazec90-%.py:
	echo -ne 'template = "map90.svg"\npic = "obrazec-$*.png"\n' > $@
aon/obrazec90-%.svg: aon/obrazec90-%.py aon/obrazec-%.png templ/map90.svg
	python3 crk.py $< > $@ || rm -f $@
aon/obrazec90-%.pdf: aon/obrazec90-%.svg
	inkscape $< --export-type=pdf --export-filename=$@ \
		    --export-area-page --export-dpi=300
release/obrazce.pdf: aon/obrazec90-lab.pdf \
		     aon/obrazec90-aon.pdf
	pdfunite $^ $@

clean-release:
	rm -f $(CARDPAGES:%.png=release/archy/%.png)
	rm -f release/archy/karty.pdf
	rmdir release/archy ||:
	rm -f $(CARDFILES)
	rmdir release/karty ||:
	rm -f $(XCF_MAPS:%=release/map-%.xcf)
	rm -f $(MAPS:%=release/map-%.png)
	rm -f release/obrazce.pdf
	rm -f release/popis-a4.{pdf,tex}
	rm -f release/popis.odt
	rm -f release/komplet.pdf
	rm -f release/sigarda.pdf
	rm -f release/speech.wav
	rm -f release/namier-stranka.pdf
	rm -f release/deloa-mapa.pdf
	rm -f release/map-L2.xcf

clean: clean-release
	rm -f $(CARDS:%=%.svg)
	rm -f $(CARDPAGES:%.png=karty/%.png)
	rm -f $(CARDPAGES:%.png=karty/%.svg)
	rm -f $(CARDPAGES:%.png=karty/%.pdf)
	rm -f $(MAPS:%=map/map-%.png)
	rm -f $(MAPS:%=map/map-%.svg)
	rm -f $(MAPS:%=map/map-%.pdf)
	rm -f $(MAPS:%=map/map90-%.py)
	rm -f $(MAPS:%=map/map90-%.svg)
	rm -f $(MAPS:%=map/map90-%.pdf)
	rm -f aon/obrazec-{aon,lab}.{png,svg}
	rm -f aon/obrazec90-{aon,lab}.{svg,pdf}
	rm -f namier/namier-stranka.{pdf,svg}
	rm -f sigarda/sigarda-chajda-mapa.png
	rm -f sigarda/sigarda90-chajda-mapa.{py,svg,pdf}
	rm -f popis-a4.{aux,log,out}
	rm -f popis.tex

aon/aon.svg: aon/aon.txt $(TR1) $(TR2) x.py y.py
	cat $< | sed -f $(TR1) | sed -f $(TR2) | python3 x.py $@
