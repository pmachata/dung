s/\b/=/g

s/mě/mně/g
s/Mě/Mně/g
s/MĚ/MNĚ/g

s/ou/ᴕ/g

s/ů/ú/g
s/Ů/Ú/g

s/di/ďy/g
s/Di/Ďy/g
s/DI/ĎY/g

s/ni/ňy/g
s/Ni/Ňy/g
s/NI/ŇY/g

s/ti/ťy/g
s/Ti/Ťy/g
s/TI/ŤY/g

s/dě/ďe/g
s/Dě/Ďe/g
s/DĚ/ĎE/g

s/tě/ťe/g
s/Tě/Ťe/g
s/TĚ/ŤE/g

s/ně/ňe/g
s/Ně/Ňe/g
s/NĚ/ŇE/g

s/ě/je/g
s/Ě/Je/g

s/á/a'/g
s/Á/A'/g

s/é/e'/g
s/É/E'/g

s/í/i'/g
s/Í/I'/g

s/ó/o'/g
s/Ó/O'/g

s/ú/u'/g
s/Ú/U'/g

s/ý/y'/g
s/Ý/Y'/g

: break_vowel_clusters
s/\([aeiouyAEIOUY]\)\('\?\)\([aeiouyAEIOUY]\)/\1\2_\3/g
t break_vowel_clusters

s/\b[aeiouyAEIOUY]/_&/g

s/č/c</g
s/Č/C</g

s/ď/d</g
s/Ď/D</g

s/ň/n</g
s/Ň/N</g

s/ř/r</g
s/Ř/R</g

s/š/s</g
s/Š/S</g

s/ť/t</g
s/Ť/T</g

s/ž/z</g
s/Ž/Z</g

s/ch/h</g
s/Ch/H</g
s/CH/H</g

y/ABCDEFGHIJKLMNOPQRSTUVWXYZ/abcdefghijklmnopqrstuvwxyz/
