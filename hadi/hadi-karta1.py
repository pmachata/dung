template = "text.svg"

name = "Had"
blurb = """

Užovka: <b>Fyzička 1, Finesa 4, Duše -</b>, Životy 6
Škrtič: <b>Fyzička 4, Finesa 1, Duše -</b>, Životy 11
<br>Zbroj 0
<br>
<br>Za úspěchy mohou znevýhodnit útočníka omotáním končetin či zbraní.

<br><br>Asi dvoumetroví hadi s řetězovým tělem. Jsou laxní, ale nenechají do
sebe rýpat. Často se vyskytují v celých klubcích.

"""
