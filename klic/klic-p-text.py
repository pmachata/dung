template = "text.svg"
background = "elfi"
pic = "klic-p-mala.png"

name = 'Velký klíč P'
blurb = """

Velký mosazný klíč. Pasuje do dveří L1D2 (dveře mezi portálovou místností a
vestibulem).

"""
