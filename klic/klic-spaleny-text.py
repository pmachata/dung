template = "text.svg"
background = "elfi"
pic = "klic-spaleny-mala.png"

name = "Spálený klíč"
blurb = """

Spálený klíč. Stejný jako Mosazný klíč, ale je spálený. Pasuje do dveří
L1D8.

"""
