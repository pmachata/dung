template = "text.svg"
background = "elfi"
pic = "klic-mosazny-mala.png"

name = "Mosazný klíč"
blurb = """

Mosazný klíč. Stejný jako Spálený klíč, ale není spálený. Pasuje do dveří L1D8.

"""
