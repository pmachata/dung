template = "text.svg"
background = "elfi"
pic = "klic-q-mala.png"

name = 'Velký klíč Q'
blurb = """

Velký mosazný klíč. Je dost omšelý, zřejmě je často používán. Pasuje do
dveří L1D6 (dveře z portálové místnosti ven).

"""
