template = "text.svg"
background = frame = "elfi"
pic = "mandakam-mala.png"

name = "Mandalový kamínek"
blurb = """

Kamínek je třeba sevřít v dlani a pronést slova <b>*magén néfesh*</b>. Poté
se okolo sesilatele rozprostře ochranná bariéra, která ochrání ty, kteří
jsou uvnitř, před duševními útoky. Bariéra pokryje všechny v kontaktní
vzdálenosti, a bude držet, dokud se sesilatel soustředí.

"""
