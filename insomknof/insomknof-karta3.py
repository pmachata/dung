template = "text.svg"
background = "elfi"
frame = "proklety"
pic = "cufflink-maly.png"

name = "Insomnický knoflík"
blurb = """

Manžetový knoflík s motivem spící tváře. Kdo jej nosí na oděvu, nepotřebuje
spát.

<br><br>Od druhé probdělé noci dále se nositel knoflíku může stát terčem
vnuknutí od sil chaosu. Vnuknutí funguje zhruba jako kouzlo sugesce nebo
hypnóza.

"""
