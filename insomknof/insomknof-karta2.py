template = "text.svg"
background = frame = "elfi"
pic = "cufflink-maly.png"

name = "Insomnický knoflík"
blurb = """

Manžetový knoflík s motivem spící tváře. Kdo jej nosí na oděvu, nepotřebuje
spát.

"""
