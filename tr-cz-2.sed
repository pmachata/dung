# this works with output of tr-cz-1.sed

s/y/i/g

s/b/ʙ/g
s/d/ᴅ/g
s/f/ɾ/g
s/g/ɢ/g
s/h/ʜ/g
s/j/ᴊ/g
s/k/ᴋ/g
s/l/ʟ/g
s/p/ᴘ/g
s/t/ᴛ/g
s/_/ː/g

s/\(.\)</<\1/g

#s/<\(.\)/\1̧/g
s/<\(.\)/\1̭/g

s/\(.\)a/\1̄/g
s/\(.\)e/\1̈/g
s/\(.\)i/\1̇/g
s/\(.\)y/\1̇/g
s/\(.\)o/\1̊/g
s/\(.\)u/\1̆/g
#s/\(.\)'/\1́/g
s/'/ː/g

s/ᴏ̊/ᴏ/g

s/=//g
