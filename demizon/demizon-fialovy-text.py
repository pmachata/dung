template = "text.svg"
background = "elfi"
pic = "demizon-fialovy-mala.png"

name = "Fialová tekutina"
blurb = """

Lahev fialové tekutiny.

<br><br>Tekutina je žíravá, při dotyku pálí. Způsobuje zranění 3kz
<jed>žíravinou</jed> každé kolo, kdy je oběť působení vystavena. Lahev
obsahuje deset dávek žíraviny.

"""
