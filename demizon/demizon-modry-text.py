template = "text.svg"
background = "elfi"
pic = "demizon-modry-mala.png"

name = "Modrá tekutina"
blurb = """

Lahev modré tekutiny.

<br><br>Páchne alkoholicky, ale po vypití se jedná o jed s Latencí 8 hodin,
Silou 2 za každých 20ml. Dá se neutralizovat stejným množstvím obyčejného
alkoholu.

"""
