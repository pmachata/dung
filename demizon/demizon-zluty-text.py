template = "text.svg"
background = "elfi"
pic = "demizon-zluty-mala.png"

name = "Žlutá tekutina"
blurb = """

Lahev žluté tekutiny.

<br><br>Tekutina je těkavá, na vzduchu se rychle vypařuje. Vdechování
způsobuje zranění 3kz <jed>žíravinou</jed> každé kolo (podle koncentrace).
Lahev obsahuje deset dávek tekutiny.

"""
