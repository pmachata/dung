template = "text.svg"
background = "elfi"
pic = "demizon-cerny-mala.png"

name = "Černá tekutina"
blurb = """

Lahev černé tekutiny.

<br><br>Tekutina silně špiní a na všem ulpívá. Dobře hoří (4kz
<ohen>ohněm</ohen> každé kolo). Lahev obsahuje deset dávek tekutiny.

"""
