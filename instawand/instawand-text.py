template = "text.svg"
frame = "elfi"
pic = "instawand-mala.png"

name = "Instantní hůlka"
blurb = """

Pomocí několikaminutového rituálu je možné této hůlce zpřístupnit libovolné
jedno připravené kouzlo. Poté je možné hůlku někomu dát, a ten může dané
kouzlo skrze hůlku jednou seslat, použitím obvyklé formule, pokud jej
hostitel kouzla má stále připravené. Pro účely seslání se počítá Duše
hostitele. Hostitel může přistup kdykoli znovu uzavřít.

"""
