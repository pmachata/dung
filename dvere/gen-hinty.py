import sys
import importlib

print("| Karta | Text |")
print("|-------+------|")

for path in sys.argv[1:]:
    assert path.endswith(".py")
    path = path[:-3]
    assert "/" not in path

    mod = importlib.import_module(path)
    print("| %s | %s |" % (mod.name, mod.hint))
