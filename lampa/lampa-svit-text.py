template = "text.svg"
frame = "elfi"
pic = "lampa-svit-mala.png"

name = "Lampa"
blurb = """

Funguje jako kouzelná hůlka. Umí seslat kouzlo Osvětli <i>*leha’ír*</i>,
které se chová podobně jako kouzelníkovo kouzlo Světlo, ale svítit bude
vždy jen krystal na vrcholu tyče, a kouzlo místo soustředění svítí tak
dlouho, dokud je sesilatel v okolí místa, kde k seslání došlo.

"""
