template = "text.svg"
frame = "trpaslici"
pic = "okular-mala.png"

name = "Okulár"
blurb = """

Okulár, který zvětšuje asi na dvojnásobek.

<br><br>Po vyslovení formule <b>*l'hitmaked*</b> zvětšuje až 10x a míru
zvětšení lze regulovat. Aktivace vydrží několik minut.

"""
